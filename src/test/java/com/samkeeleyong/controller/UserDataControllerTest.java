package com.samkeeleyong.controller;

import static com.samkeeleyong.domain.DateComparison.GREATER_THAN_OR_EQUAL;
import static com.samkeeleyong.domain.DateComparison.LESS_THAN_OR_EQUAL;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.jpa.domain.Specification;

import com.samkeeleyong.domain.UserData;
import com.samkeeleyong.domain.UserDataRepository;
import com.samkeeleyong.dto.form.LoggedInSearchRequest;
import com.samkeeleyong.dto.view.UserDataView;
import com.samkeeleyong.utils.SpecificationUtils;

@RunWith(MockitoJUnitRunner.class)
public class UserDataControllerTest {

	@InjectMocks
	private UserDataController controller;
	
	@Mock
	private UserDataRepository userDataRepository;
	
	@Test
	public void testFindUniqueData() {

		// given
		List<UserData> mockListOfUsers = getMockListOfUsers();
		Mockito.when(userDataRepository.findUserLoginWith(Mockito.any(LocalDateTime.class), 
														  Mockito.any(LocalDateTime.class)))
										.thenReturn(mockListOfUsers);
		
		// when
		List<UserDataView> result = controller.findUniqueUserData("19940122", "20181212");
		
		// then
		assertNotNull(result);
		assertEquals(3, result.size());
		Mockito.verify(userDataRepository, Mockito.times(1))
			   .findUserLoginWith(Mockito.any(LocalDateTime.class), Mockito.any(LocalDateTime.class));
	}
	
	@Test
	public void testRetrieveListOfUniqueDates() {
		// given
		List<String> listOfStrings = Arrays.asList(new String[] {"19940101", "19940202"});
		Mockito.when(userDataRepository.findUniqueDates())
										.thenReturn(listOfStrings);
		
		// when
		List<String> result = controller.retrieveListOfUniqueDates();
		
		// then
		assertNotNull(result);
		assertEquals(2, result.size());
		assertTrue(result.contains("19940101"));
		assertTrue(result.contains("19940202"));
		Mockito.verify(userDataRepository, Mockito.times(1))
			   .findUniqueDates();
	}

	@SuppressWarnings("deprecation")
	@Test
	public void testFindUserData() {
		// given
		LoggedInSearchRequest request = new LoggedInSearchRequest();
		List<UserData> mockListOfUsers = getMockListOfUsers();
		Mockito.when(userDataRepository.findAll((Specification)null))
										.thenReturn(mockListOfUsers);
		
		// when
		Map<String, Long> result = controller.findUserData(request);
		
		// then
		Mockito.verify(userDataRepository, Mockito.times(1))
			   .findAll((Specification)null);
	}
	
	private List<UserData> getMockListOfUsers() {
		List<UserData> list = new ArrayList<>();
		
		list.add(new UserData("user1", "attribute1", "attribute2", "attribute3", "attribute4", LocalDateTime.now()));
		list.add(new UserData("user2", "attribute1", "attribute2", "attribute3", "attribute4", LocalDateTime.now()));
		list.add(new UserData("user3", "attribute1", "attribute2", "attribute3", "attribute4", LocalDateTime.now()));
		
		return list ;
	}
}
