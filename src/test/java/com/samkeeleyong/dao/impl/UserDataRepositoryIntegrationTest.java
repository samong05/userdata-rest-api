package com.samkeeleyong.dao.impl;

import static com.samkeeleyong.domain.DateComparison.GREATER_THAN_OR_EQUAL;
import static com.samkeeleyong.domain.DateComparison.LESS_THAN_OR_EQUAL;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.samkeeleyong.domain.UserData;
import com.samkeeleyong.domain.UserDataRepository;
import com.samkeeleyong.dto.form.LoggedInSearchRequest;
import com.samkeeleyong.utils.SpecificationUtils;

@DataJpaTest
@RunWith(SpringRunner.class)
@Transactional
@ActiveProfiles("test")
public class UserDataRepositoryIntegrationTest {

	@Autowired
	private UserDataRepository userDataRepository;

	@Test
	public void testUsersWithLogin_completeArguments() {
		List<UserData> users = userDataRepository.findUserLoginWith(getDateFromString("2018-08-03 09:09"), 
																	getDateFromString("2018-08-05 13:13"));
		assertNotNull(users);
		assertEquals(3, users.size());
	}

	@Test
	public void testUsersWithLogin_nullEnd() {
		List<UserData> users = userDataRepository.findUserLoginWith(getDateFromString("2018-08-04 08:08"),
																	null);
		assertNotNull(users);
		assertEquals(8, users.size());
	}
	
	@Test
	public void testUsersWithLogin_nullStart() {
		List<UserData> users = userDataRepository.findUserLoginWith(null,
																	getDateFromString("2018-08-04 15:15"));
		assertNotNull(users);
		assertEquals(2, users.size());
	}
	
	@Test
	public void testUsersWithLogin_nullArguments() {
		List<UserData> users = userDataRepository.findUserLoginWith(null, null);
		List<UserData> allUsers = userDataRepository.findAll();
		assertNotNull(users);
		assertEquals(allUsers.size(), users.size());
	}
	
	@Test
	public void testFindUniqueDates() {
		List<String> uniqueDates = userDataRepository.findUniqueDates();
		assertNotNull(uniqueDates);
		
		long countOfDate = uniqueDates.stream().filter(dateString -> dateString.equals("2099-08-05")).count();
		assertEquals(1, countOfDate);
	}
	
	@Test
	public void testFindAllWithSpec() {
		
		LoggedInSearchRequest request = new LoggedInSearchRequest();
		request.setStart("20180805");
		request.setEnd("20180807");
		request.setAttribute1(new String[] {"RED"});
		request.setAttribute2(new String[] {"Peso"});
		request.setAttribute3(new String[] {"mastercard"});
		request.setAttribute4(new String[] {"Sales", "Management"});
		List<UserData> result = userDataRepository.findAll(createSpecFromRequest(request));

		assertNotNull(result);
		
		long countOfJuans = result.stream().filter(data -> data.getUser().equals("Juan Dela Cruz")).count();
		assertEquals(2, countOfJuans);
	}
	
	@Test
	public void testFindAllWithIncompleteSpec() {
		
		LoggedInSearchRequest request = new LoggedInSearchRequest();
		request.setStart("20180805");
		request.setAttribute1(new String[] {"RED"});
		List<UserData> result = userDataRepository.findAll(createSpecFromRequest(request));

		assertNotNull(result);
		
		long countOfJuans = result.stream().filter(data -> data.getUser().equals("Juan Dela Cruz")).count();
		assertEquals(3, countOfJuans);
	}
	
	// copied from controller
    private Specification<UserData> createSpecFromRequest(LoggedInSearchRequest request) {
    	Specification<UserData> specToReturn =  null;
    	
    	specToReturn = SpecificationUtils.specFromTruncatedDate(specToReturn, "loginDateTime", request.getStart(), GREATER_THAN_OR_EQUAL);
    	specToReturn = SpecificationUtils.specFromTruncatedDate(specToReturn, "loginDateTime", request.getEnd(), LESS_THAN_OR_EQUAL);
    	
    	specToReturn = SpecificationUtils.specFromArrayOfAttr(specToReturn, "attribute1", request.getAttribute1());
    	specToReturn = SpecificationUtils.specFromArrayOfAttr(specToReturn, "attribute2", request.getAttribute2());
    	specToReturn = SpecificationUtils.specFromArrayOfAttr(specToReturn, "attribute3", request.getAttribute3());
    	specToReturn = SpecificationUtils.specFromArrayOfAttr(specToReturn, "attribute4", request.getAttribute4());
    	
    	return specToReturn;
    }
	
	// TODO if have time, use db-unit
	@Before
	public void setupData() {
		userDataRepository.deleteAll();
		saveData("sam", "attribute1", "attribute2", "attribute3", "attribute4", "2018-08-03 10:30");
		saveData("sam2", "attribute1", "attribute2", "attribute3", "attribute4", "2018-08-04 11:30");
		saveData("Juan Dela Cruz", "RED", "Peso", "mastercard", "Management", "2018-08-05 12:30");
		saveData("Juan Dela Cruz", "RED", "Peso", "mastercard", "Sales", "2018-08-07 12:30");
		saveData("sam4", "attribute1", "attribute2", "attribute3", "attribute4", "2018-08-06 13:30");
		saveData("Juan Dela Cruz", "BLUE", "Peso", "visa", "Consulting", "2018-08-07 13:30");
		saveData("Juan Dela Cruz", "RED", "Peso", "visa", "HR", "2018-08-08 13:30");
		
		saveData("sam99", "attribute1", "attribute2", "attribute3", "attribute4", "2099-08-05 13:30");
		saveData("sam100", "attribute1", "attribute2", "attribute3", "attribute4", "2099-08-05 13:30");
		userDataRepository.flush();
	}
	
	private void saveData(String name, String attr1, String attr2, String attr3, String attr4, String dateString) {
		userDataRepository.save(new UserData(name, attr1, attr2, attr3, attr4, getDateFromString(dateString)));
	}

	private LocalDateTime getDateFromString(String dateString) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
		LocalDateTime formatDateTime = LocalDateTime.parse(dateString, formatter);
		return formatDateTime;
	}
}
