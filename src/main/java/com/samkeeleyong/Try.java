package com.samkeeleyong;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class Try {

	private static String[] arr = new String[] {"2"}; 
	public static void main(String[] args) throws FileNotFoundException {
		Scanner scanner = new Scanner(new File("C:\\Users\\Sam\\Downloads\\myschema.csv"));
		
		List<String> nameSet = new ArrayList<>();
		List<String> colourSet = new ArrayList<>();
		List<String> currencySet = new ArrayList<>();
		List<String> cardSet = new ArrayList<>();
		List<String> departmentSet = new ArrayList<>();
		
		while(scanner.hasNext()) {
			String[] lineSplit = scanner.nextLine().split(",");
			
			nameSet.add(lineSplit[1]);
			colourSet.add(lineSplit[3]);
			currencySet.add(lineSplit[4]);
			cardSet.add(lineSplit[5]);
			departmentSet.add(lineSplit[6]);
		}
		
		/*System.out.println(nameSet.size());
		System.out.println(nameSet);
		System.out.println(colourSet.size());
		System.out.println(colourSet);
		System.out.println(cardSet.size());
		System.out.println(cardSet);
		System.out.println(departmentSet.size());
		System.out.println(departmentSet)*/;
		
		String format = "INSERT INTO data (id, user, attribute1, attribute2, attribute3, attribute4, login_time) VALUES ('%s', '%s', '%s', '%s','%s','%s','%s')";
		for (int i = 1; i <= 100_000; i++) {
			String insertString = String.format(format, Integer.toString(i), 
						  nameSet.get(new Random().nextInt(nameSet.size())),
						  colourSet.get(new Random().nextInt(colourSet.size())),
						  currencySet.get(new Random().nextInt(currencySet.size())),
						  cardSet.get(new Random().nextInt(cardSet.size())),
						  departmentSet.get(new Random().nextInt(departmentSet.size())),
						  getRandomTimeStamp()
					);
			System.out.println(insertString);
		}
	}

	private static String getRandomTimeStamp() {
		String dateString = randBetween(1980, 2018) + "-" + String.format("%02d", randBetween(1, 12)) + "-" + String.format("%02d", randBetween(1, 28)) + " " + 
							randBetween(0, 23) + ":" + randBetween(0, 59) + ":" + randBetween(0, 59);
		return dateString;
	}
	
	public static int randBetween(int start, int end) {
        return start + (int)Math.round(Math.random() * (end - start));
    }
}
//INSERT INTO data (id, user, attribute1, attribute2, attribute3, attribute4, login_time) VALUES ('1', );