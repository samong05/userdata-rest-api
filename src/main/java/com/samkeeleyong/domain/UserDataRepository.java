package com.samkeeleyong.domain;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface UserDataRepository extends JpaRepository<UserData, String>, JpaSpecificationExecutor<UserData> {

	@Query("SELECT data FROM UserData data WHERE (data.loginDateTime >= :startFilter OR :startFilter is null) "
			+ " AND (data.loginDateTime <= :endFilter OR :endFilter is null)")
	List<UserData> findUserLoginWith(@Param("startFilter") LocalDateTime startFilter, 
									 @Param("endFilter") LocalDateTime endFilter);
	
	@Query(value = "SELECT DISTINCT TO_CHAR(CAST(login_time as DATE), 'YYYY-MM-DD') unique_date FROM DATA",
		   nativeQuery = true)
	List<String> findUniqueDates();
}
