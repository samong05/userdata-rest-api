package com.samkeeleyong.domain;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "data")
public class UserData {

	@Id
	@GeneratedValue(generator = "UUID")
	@GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
	private String id;
	
	@Column
	private String user;
	
	@Column
	private String attribute1;
	
	@Column
	private String attribute2;
	
	@Column
	private String attribute3;
	
	@Column
	private String attribute4;
	
	@Column(name="login_time")
	private LocalDateTime loginDateTime;

	public UserData(String user, String attribute1, String attribute2, 
					String attribute3, String attribute4, LocalDateTime loginDateTime) {
		super();
		this.user = user;
		this.attribute1 = attribute1;
		this.attribute2 = attribute2;
		this.attribute3 = attribute3;
		this.attribute4 = attribute4;
		this.loginDateTime = loginDateTime;
	}

	@Override
	public String toString() {
		return "UserData [user=" + user + ", loginDateTime=" + loginDateTime + "]";
	}

	// getters and setters
	public String getId() {
		return id;
	}
	
	public String getUser() {
		return user;
	}

	public String getAttribute1() {
		return attribute1;
	}

	public String getAttribute2() {
		return attribute2;
	}

	public String getAttribute3() {
		return attribute3;
	}

	public String getAttribute4() {
		return attribute4;
	}

	public LocalDateTime getLoginDateTime() {
		return loginDateTime;
	}

	// required by jpa
	protected UserData() {
		
	}
}
