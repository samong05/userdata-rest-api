package com.samkeeleyong.domain;

public enum DateComparison {

	GREATER_THAN_OR_EQUAL,
	LESS_THAN_OR_EQUAL,
	;
}
