package com.samkeeleyong.dto.view;

import com.samkeeleyong.domain.UserData;

public class UserDataView {

	private String loginDate;
	
	private String user;
	
	private String attribute1;
	
	private String attribute2;
	
	private String attribute3;
	
	private String attribute4;

	public UserDataView(UserData data) {
		this(data.getLoginDateTime().toString(), data.getUser(), data.getAttribute1(), 
			 data.getAttribute2(), data.getAttribute3(), data.getAttribute4());
	}
	
	public UserDataView(String loginDate, String user, String attribute1, 
				String attribute2, String attribute3, String attribute4) {
		super();
		
		this.loginDate = loginDate;
		this.user = user;
		this.attribute1 = attribute1;
		this.attribute2 = attribute2;
		this.attribute3 = attribute3;
		this.attribute4 = attribute4;
	}

	// getters 	
	public String getLoginDate() {
		return loginDate;
	}

	public String getUser() {
		return user;
	}

	public String getAttribute1() {
		return attribute1;
	}

	public String getAttribute2() {
		return attribute2;
	}

	public String getAttribute3() {
		return attribute3;
	}

	public String getAttribute4() {
		return attribute4;
	}	
}
