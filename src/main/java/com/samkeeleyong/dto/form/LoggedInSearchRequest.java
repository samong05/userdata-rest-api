package com.samkeeleyong.dto.form;

import java.util.Arrays;

public class LoggedInSearchRequest {

	private String start;
	private String end;
	private String[] attribute1;
	private String[] attribute2;
	private String[] attribute3;
	private String[] attribute4;

	@Override
	public String toString() {
		return "LoggedInSearchRequest [start=" + start + ", end=" + end + ", attribute1=" + Arrays.toString(attribute1)
				+ ", attribute2=" + Arrays.toString(attribute2) + ", attribute3=" + Arrays.toString(attribute3)
				+ ", attribute4=" + Arrays.toString(attribute4) + "]";
	}

	public String getStart() {
		return start;
	}
	
	public void setStart(String start) {
		this.start = start;
	}
	
	public String getEnd() {
		return end;
	}
	
	public void setEnd(String end) {
		this.end = end;
	}
	
	public String[] getAttribute1() {
		return attribute1;
	}
	
	public void setAttribute1(String[] attribute1) {
		this.attribute1 = attribute1;
	}
	
	public String[] getAttribute2() {
		return attribute2;
	}
	
	public void setAttribute2(String[] attribute2) {
		this.attribute2 = attribute2;
	}
	
	public String[] getAttribute3() {
		return attribute3;
	}
	
	public void setAttribute3(String[] attribute3) {
		this.attribute3 = attribute3;
	}
	
	public String[] getAttribute4() {
		return attribute4;
	}
	
	public void setAttribute4(String[] attribute4) {
		this.attribute4 = attribute4;
	}
}
