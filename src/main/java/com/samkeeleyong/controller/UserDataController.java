package com.samkeeleyong.controller;

import static com.samkeeleyong.domain.DateComparison.GREATER_THAN_OR_EQUAL;
import static com.samkeeleyong.domain.DateComparison.LESS_THAN_OR_EQUAL;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.samkeeleyong.domain.UserData;
import com.samkeeleyong.domain.UserDataRepository;
import com.samkeeleyong.dto.form.LoggedInSearchRequest;
import com.samkeeleyong.dto.view.UserDataView;
import com.samkeeleyong.utils.SpecificationUtils;

@RestController
@RequestMapping(value = "/test")
public class UserDataController {

	private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("yyyyMMdd");
	private UserDataRepository userDataRepository;

	@Autowired
	public UserDataController(UserDataRepository userDataRepository) {
		this.userDataRepository = userDataRepository;
	}

	/**
	 * http://server/test/users?start=YYYYMMDD&end=YYYYMMDD 
	 *
	*  Retrieves a JSON array of all the unique users for which there is a login record between the start and end date.
	 * Both parameters are optional, so there can be a start date, an end date, or both.
	 * The resulting JSON array needs to be sorted ascending.
	*/
    @RequestMapping(value = "/users") 
    public List<UserDataView> findUniqueUserData(@RequestParam(value="start", required = false) String startFilter,
    										 @RequestParam(value="end", required = false) String endFilter) {
    	LocalDateTime startDateFilter = convertToLocalDateTime(startFilter, "start");
    	LocalDateTime endDateFilter = convertToLocalDateTime(endFilter, "end");
    	
    	return userDataRepository.findUserLoginWith(startDateFilter, endDateFilter)
    							 .stream().map(UserDataView::new)
    							 .collect(Collectors.toList());
    }
    
    private LocalDateTime convertToLocalDateTime(String dateString, String compare) {
    	if (dateString == null) {
    		return null;
    	}
    	
    	// this could have been done better
    	if ("start".equals(compare)) {
    		return LocalDate.parse(dateString, DATE_FORMATTER).atStartOfDay();
    	} else {
    		return LocalDate.parse(dateString, DATE_FORMATTER).atTime(23, 59, 59);    		
    	}
	}
    
    /** 
     * @return Retrieves a JSON array of all the unique dates (ignoring time) in the table
     * 		   The resulting JSON array needs to be sorted ascending.
     */
    @RequestMapping(value = "/dates")
    public List<String> retrieveListOfUniqueDates() {
    	return userDataRepository.findUniqueDates();
    }
    
    /**
     *  http://server/test/logins?start=YYYYMMDD&end=YYYYMMDD&attribute1=AAA&attribute2=BBB&attribute3=CCC&attribute4=DDD
     *  Retrieves a JSON object where the key is the user name and the value is the number of times a user logged on between the start and the end date.
     *	All parameters are optional.
     *	The values used for the attributes are used as filters, i.e. only the records should be counted for which the attribute values are equal to the ones specified in the parameters.
     *	For one attribute, multiple values might be present, e.g. http://server/test/logins?attribute1=AA1&
     *																						attribute1=AA2&
     *																						attribute1=AA3 
     */
    @RequestMapping(value = "/logins")
    public Map<String, Long> findUserData(LoggedInSearchRequest request) {
    	List<UserData> search = userDataRepository.findAll(createSpecFromRequest(request));

    	return search.stream().collect(Collectors.groupingBy(UserData::getUser, Collectors.counting()));
    }
    
    private Specification<UserData> createSpecFromRequest(LoggedInSearchRequest request) {
    	Specification<UserData> specToReturn =  null;
    	
    	specToReturn = SpecificationUtils.specFromTruncatedDate(specToReturn, "loginDateTime", request.getStart(), GREATER_THAN_OR_EQUAL);
    	specToReturn = SpecificationUtils.specFromTruncatedDate(specToReturn, "loginDateTime", request.getEnd(), LESS_THAN_OR_EQUAL);
    	
    	specToReturn = SpecificationUtils.specFromArrayOfAttr(specToReturn, "attribute1", request.getAttribute1());
    	specToReturn = SpecificationUtils.specFromArrayOfAttr(specToReturn, "attribute2", request.getAttribute2());
    	specToReturn = SpecificationUtils.specFromArrayOfAttr(specToReturn, "attribute3", request.getAttribute3());
    	specToReturn = SpecificationUtils.specFromArrayOfAttr(specToReturn, "attribute4", request.getAttribute4());
    	
    	return specToReturn;
    }
}
