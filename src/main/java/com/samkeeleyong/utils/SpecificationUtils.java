package com.samkeeleyong.utils;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import javax.persistence.criteria.Expression;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.StringUtils;

import com.samkeeleyong.domain.DateComparison;
import com.samkeeleyong.domain.UserData;

public final class SpecificationUtils {

	private static final String CONVERT_DATE_SQL_FUNCTION = "TO_DATE";
	private static final String TRUNC_SQL_FUNCTION = "TRUNC";
	private static final String DEFAULT_DATE_ARG_FORMAT = "YYYYMMDD";

	private SpecificationUtils() {}
	
	/**
	 * 
	 * @param existingSpec
	 * @param specification
	 * @return
	 */
	public static <T> Specification<T> and(Specification<T> existingSpec, Specification<T> specification) {

        if (existingSpec == null) {
        	existingSpec = Specification.where(specification);
        } else {
        	existingSpec = existingSpec.and(specification);
        }

        return existingSpec;
    }
	
	 /**
	  *
	  * @param attributeName name of <b>first-level</b> attribute
	  * @param actualStringValue value to use in <b>where clause</b> with
	  *
	  * @return Specification object for query use
	  */
	public static <T> Specification<T> whereSingleAttributeIs(Specification<T> existingSpec,
															  final String attributeName,
															  final String actualStringValue) {
		return and(existingSpec, (root, query, cb) -> cb.equal(root.get(attributeName), actualStringValue));
	}
	
	public static <T> Specification<T> whereSingleAttributeIsInArray(Specification<T> existingSpec,
			  												  final String attributeName,
			  												  final List<String> actualListOfvalues) {
		return and(existingSpec, (root, query, cb) -> root.get(attributeName).in(actualListOfvalues));
	}

	public static <T> Specification<T> specFromArrayOfAttr(Specification<T> existingSpec, final String attributeName, 
														   final String[] arrOfValues) {
		if (arrOfValues == null) {
			return existingSpec;
		}

		existingSpec = SpecificationUtils.whereSingleAttributeIsInArray(existingSpec, attributeName, Arrays.asList(arrOfValues));
		
		return existingSpec;
	}

	public static Specification<UserData> specFromTruncatedDate(Specification<UserData> existingSpec, final String attributeName,
																final String actualArgument, final DateComparison dateComparison) {
		return specFromTruncatedDate(existingSpec, attributeName, actualArgument, DEFAULT_DATE_ARG_FORMAT, dateComparison);
	}
	
	public static Specification<UserData> specFromTruncatedDate(Specification<UserData> existingSpec, final String attributeName,
																final String actualArgument, final String dateFormatOfArgument,
																final DateComparison dateComparison) {
		
		if (StringUtils.isEmpty(actualArgument)) {
			return existingSpec;
		}

		return SpecificationUtils.and(existingSpec, (root, query, cb) -> {
			final Expression<LocalDate> truncatedDateFromDb = cb.function(TRUNC_SQL_FUNCTION, LocalDate.class, 
																	root.get(attributeName));
			final Expression<LocalDate> dateArgument = cb.function(CONVERT_DATE_SQL_FUNCTION, LocalDate.class, 
																cb.literal(actualArgument), cb.literal(dateFormatOfArgument));
			
			if (dateComparison == DateComparison.GREATER_THAN_OR_EQUAL) {
				return cb.greaterThanOrEqualTo(truncatedDateFromDb, dateArgument);				
			} else if (dateComparison == DateComparison.LESS_THAN_OR_EQUAL) {
				return cb.lessThanOrEqualTo(truncatedDateFromDb, dateArgument);
			} else {
				throw new UnsupportedOperationException("Date Comparison method not supported.");
			}
		});
	}

}
